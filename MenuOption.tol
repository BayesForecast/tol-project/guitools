Class @MenuOption {
  // Nombre de la opcion. Se sigue el convenio
  // "parent1/parent2/option" para indicar que "parent1" es un submenu
  // que contiene al submenu "parent2" y este a su vez la opcion
  // "option"
  Text _.name;
  // Etiqueta a mostrar en el menu, por omision toma el valor de _.name
  Text _.label;
  // nombre de la imagen a mostrar, por omision no tiene imagen
  // asociada.
  Text _.image = "";
  // flag de traduccion, por omision traduce. Por ahora tenemos un
  // problema ya que la representacion de las cadenas en tol no
  // admiten multibyte requerido en traducciones. Basado en este
  // atributo el GUI hara la traduccion apropiada.
  Real _.flagTranslate = 0;

  // el campo rank se utiliza para establecer un orden dentro del menu
  Real _.rank = 0;

  Set getEntryInfo( Real void )
  {
    Set _getEntryInfo(?)
  };

  Set _getEntryInfo( Real void )
  {
    Set [[ _.name, _.label, _.image, _.flagTranslate, _.rank ]]
  };

  Real replace( NameBlock from )
  {
    _replaceOption( NameBlockToSet( from ) )
  };

  Real _replaceOption( Set from )
  {
    Text _.name := args::name;
    Text _.label := _getReplaceField( from, "label" );
    Text _.image := _getReplaceField( from, "image" );
    Real _.flagTranslate := _getReplaceField( from, "flagTranslate" );
    Real _.rank := _getReplaceField( from, "rank" )
  };

  Anything _getReplaceField( Set from, Text field )
  {
    Real idx = FindIndexByName( from, field );
    Anything If( idx, from[idx], Eval( "_." << field ) )
  };
  
  Static @MenuOption New( NameBlock args )
  {
    @MenuOption
      [[
        Text _.name = args::name;
        Text _.label = getOptArg( args, "label", args::name );
        Text _.image = getOptArg( args, "image", "" );
        Real _.flagTranslate = getOptArg( args, "flagTranslate", 0 );
        Real _.rank = getOptArg( args, "rank", 0 )
      ]]
  };

  Static Set getDefaultEntryInfo( Text optionName )
  {
    Set items = Tokenizer( optionName, "/" );
    Text _.name = optionName;
    Text _.label = items[ Card( items ) ];
    Text _.image = "";
    Real _.flagTranslate = 0;
    Real _.rank = 0;
    Set [[ _.name, _.label, _.image, _.flagTranslate, _.rank ]]
  }
};

Class @MenuCommand : @MenuOption {
  // flag de opcion de grupo, por omision es individual.
  Real _.flagGroup = 0;
  // conjunto de comandos asociado a la opcion de menu. Los comandos
  // reconocidos son:
  //  CmdInvoke: funcion que se invoca al seleccionar la opcione de menu
  //  CmdCheckState: funcion que se invoca para determinar el estado de la opcion.
  Set _.commands = Copy( Empty );
  // Argumentos extras que se pasaran a los comandos en la ultima
  // posicion de la lista e argumentos.
  Set _.extraData;

  Text _.delegateOn = "";

  Set getEntryInfo( Real void )
  {
    Set _getEntryInfo(?) << [[ _.flagGroup, _.delegateOn ]]
  };

  Real isMultiple( Real void )
  {
    Real _.flagGroup
  };

  Real invoke( Text delegateOn, Anything objOrSelection )
  {
    Real idx = FindIndexByName( _.commands, "CmdInvoke" );
    If( idx, {
        Code actionInvoke = _.commands[idx];
        // Set View( [[ actionInvoke ]], "Std" );
        Real actionInvoke( _getDelegateOnTarget( delegateOn, objOrSelection ),
                           _.extraData )
      }, {
        Warning( "No se ha podido invocar la accion ya que CmdInvoke no esta definido para " + _.name );
        0
      } )
  };

  Real checkState( Text delegateOn, Anything objOrSelection )
  {
    Real idx = FindIndexByName( _.commands, "CmdCheckState" );
    // si CmdState no esta definido asumo estado "normal"
    If( idx, {
        Code actionState = _.commands[idx];
        Real actionState( _getDelegateOnTarget( delegateOn, objOrSelection ), _.extraData )
      }, 1 )
  };

  Static Real doNothing( Anything any, Set extraData )
  {
    1
  };

  Anything getDelegateOnTarget( Anything objOrSelection )
  {
    _getDelegateOnTarget( _.delegateOn, objOrSelection )
  };
  
  Anything _getDelegateOnTarget( Text delegateOn, Anything objOrSelection )
  {
    If( delegateOn == "", objOrSelection,
        If( Grammar( objOrSelection ) == "NameBlock",
            Eval( "objOrSelection::" + delegateOn ),
            Set EvalSet( objOrSelection, Anything( NameBlock instance ) {
                Eval( "instance::" + delegateOn )
              } ) ) )
  };

  Real replace( NameBlock from )
  {
    Set _from = NameBlockToSet( from );
    _replaceOption( _from );
    _replaceCommand( _from )
  };

  Real _replaceCommand( Set from )
  {
    Real _.flagGroup := _getReplaceField( from, "flagGroup" );
    Text _.delegateOn := _getReplaceField( from, "delegateOn" );
    Set  _.extraData := _getReplaceField( from, "extraData" );
    Set _.commands := If( _.delegateOn == "", {
        [[ Code CmdInvoke = Case(
                                 ObjectExist( "Code", "from::CmdInvoke" ),
                                 from::CmdInvoke,
                                 ObjectExist( "Set", "from::CmdInvoke" ),
                                 (from::CmdInvoke)[1],
                                 1 == 1, @MenuCommand::doNothing ),
           Code CmdCheckState = Case(
                                     ObjectExist( "Code", "from::CmdCheckState" ),
                                     from::CmdCheckState,
                                     ObjectExist( "Set", "from::CmdCheckState" ),
                                     (from::CmdCheckState)[1],
                                     1 == 1, @MenuCommand::doNothing )
           ]] }, Empty );
    Real 1
  };


  Static @MenuCommand New( NameBlock args )
    {
      @MenuCommand inst =
        [[
          Text _.name = args::name;
          Text _.label = getOptArg( args, "label", args::name );
          Text _.image = getOptArg( args, "image", "" );
          Real _.flagTranslate = getOptArg( args, "flagTranslate", 0 );
          Real _.flagGroup = getOptArg( args, "flagGroup", 0 );
          Real _.rank = getOptArg( args, "rank", 0 );
          Text _.delegateOn = getOptArg( args, "delegateOn", "" );
          Set  _.commands = If( _.delegateOn == "", {
              [[ Code CmdInvoke = Case(
                                    ObjectExist( "Code", "args::CmdInvoke" ),
                                    args::CmdInvoke,
                                    ObjectExist( "Set", "args::CmdInvoke" ),
                                    (args::CmdInvoke)[1],
                                    1 == 1, @MenuCommand::doNothing ),
                 Code CmdCheckState = Case(
                                        ObjectExist( "Code", "args::CmdCheckState" ),
                                        args::CmdCheckState,
                                ObjectExist( "Set", "args::CmdCheckState" ),
                                        (args::CmdCheckState)[1],
                                        1 == 1, @MenuCommand::doNothing )
              ]] }, Empty );
          Set  _.extraData = DeepCopy( getOptArg( args, "extraData", Empty ) )
        ]]
    }
};
