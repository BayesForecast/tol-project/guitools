package require Toltcl
package require BWidget
package require snit
package require wtree
package require msgcat

namespace import -force ::msgcat::*

snit::widget ContainerEditor {

  component tree -public tree
  variable treeData

  option -objref -default "" -readonly 1
  option -translatename -default 0
  option -showbuttons -default 1 -configuremethod _ConfShowButtons
  option -checkchanges -default 1 -validatemethod BooleanOption
  option -tolcommit -default ""

  delegate option * to hull
  delegate method * to hull

  delegate option -filter to tree

  method BooleanOption {option value} {
      if {![string is boolean -strict $value]} {
          error "expected a boolean value, got \"$value\""
      }
  }

  constructor { args } {
    install tree using wtree $win.tree
    $tree configure -table yes -filter no -columns {
      { {image text} -tags NAME -label "Name" }
      { {text} -tags STATUS -label "Value" -editable yes }
    }
    grid $tree -row 0 -column 0 -sticky "snew"
    grid rowconfigure $win 0 -weight 1
    grid columnconfigure $win 0 -weight 1
    set fb [ frame $win.fbutton ]
    grid $fb -row 1 -column 0 -sticky snew
    button $fb.ok -text [ mc "Accept" ] -command [ mymethod OnAccept ]
    grid $fb.ok -row 0 -column 0
    button $fb.cancel -text [ mc "Cancel" ] -command [ mymethod OnCancel ]
    grid $fb.cancel -row 0 -column 1
    $self configurelist $args
    $self FillTree
  }

  destructor {
  }

  method OnAccept { } {
    catch {
      # protected because wtree may not implement AcceptEdit
      $tree AcceptEdit
    }
    if { $options(-tolcommit) ne "" } {
      # evaluate the tol code provided in -tolcommit option
      set addr [ tol::info address $options(-objref) ]
      toltcl::eval [ string map [ list \
                                      %C $options(-tolcommit) \
                                      %O $addr \
                                      %V [ $self GetFieldsAsTol ] ] {
        %C( GetObjectFromAddress( "%O" ), %V )
      } ]
    }
    event generate $win <<OnAccept>>
  }

  method OnCancel { } {
    catch {
      # protected because wtree may not implement CancelEdit
      $tree CancelEdit
    }
    if { $options(-checkchanges) && ![ $self _CheckChanges ] } {
      return
    }
    event generate $win <<OnCancel>>
  }

  method GetFieldsAsTol { } {
    set first 1
    set tolexpr ""
    foreach nid [ $tree item children root ] {
      if { !$first } {
        append tolexpr ", "
      } else {
        set first 0
      }
      set name [ $tree item text $nid 0 ]
      set value [ $tree item text $nid 1 ]
      array set field $treeData(dictmember,$nid)
      append tolexpr [ ContainerEditor TolObjExpr $field(grammar) $name $value ]
    }
    if { $treeData(grammar,root) eq "NameBlock" } {
      return "NameBlock \[\[ $tolexpr \]\]"
    } else {
      return "SetOfAnything( $tolexpr )"
    }
  }

  method _CheckChanges { } {
    #parray treeData
    foreach nid [ $tree item children root ] {
      set current [ $tree item text $nid 1 ]
      array set original $treeData(dictmember,$nid)
      if { $original(grammar) eq "Text" } {
        set ocontent [ string trim $original(content) \" ]
      } else {
        set ocontent $original(content)
      }
      if { $current ne $ocontent } {
        #puts "$current ne $ocontent"
        set ans [ MessageDlg $win.ask -title [ mc "Confirmation" ] \
                  -icon warning \
                  -type okcancel -aspect 75 \
                  -message [ mc "There are fields changed. Really cancel?" ] ]
        # ans == 0 means ok to cancel
        # ans == 1 means keep editing
        return [ expr { !$ans } ]
      }
    }
    return 1
  }

  method _ConfShowButtons { o v } {
    if { ![ string is boolean $v ] } {
      error [ mc "invalid boolean value '%s'", $v ]
    }
    set options(-showbuttons) $v
    if { $v } {
      grid $win.fbutton
    } else {
      grid remove $win.fbutton
    }
  }

  method FillTree { } {
    if { $options(-objref) eq "" } {
      error [ mc "invalid object reference %s" $options(-objref) ]
    }
    array unset treeInfo
    $tree item delete all
    set containerInfo [ tol::info var $options(-objref) ]
    set grammar [ lindex $containerInfo 0 ]
    # type is a reserved word within a snit type/widget
    set _type [ lindex $containerInfo 8 ]
    if { $grammar ne "NameBlock" && $grammar ne "Set" } {
      error [ mc "'%s' is not a valid container Set or NameBlock" \
                  $options(-objref) ]
    }
    set treeData(grammar,root) $grammar
    tol::foreach m $options(-objref) {
      array set member $m
      set name $member(name)
      set mgra $member(grammar)
      if { $mgra eq "Code" || $mgra eq "Anything" } {
        continue
      }
      if { $grammar eq "NameBlock" && [ regexp ^_(.*)$ $name ] } {
        # it is a hidden field
        continue
      }
      if { [ regexp ^_.autodoc.member.(.+)$ $name ==> n ] } {
        set treeData(autodoc,$n) $member(content)
        continue
      }
      set labelName [ expr  { $options(-translatename) ?
                              [ mc $name ] : $name } ]
      if { $member(grammar) eq "Text" } {
        set content [ string trim $member(content) \" ]
      } else {
        set content $member(content)
      }
      set nid [ $tree insert \
                    [ list [ list "" $labelName ] [ list $content ] ] ]
      set treeData(dictmember,$nid) $m
    }
  }

  typevariable topCounter 0
  typevariable modalResult ""

  # this method should be part of toltcl
  typemethod TolObjExpr { grammar name value } {
    switch $grammar {
      Anything -
      Code -
      Matrix -
      VMatrix -
      Serie -
      TimeSet -
      Set {
        set tolexpr ""
      }
      Text {
        set tolexpr [ expr { $name eq ""
                             ? "Text \"$value\""
                             : "Text $name = \"$value\""  } ]
      }
      Real -
      Date -
      Polyn -
      Ratio -
      Complex {
        set tolexpr [ expr { $name eq ""
                             ? "$grammar $value"
                             : "$grammar $name = $value"  } ]
      }
    }
  }

  typemethod OnAccept { top w } {
    set modalResult [ $w GetFieldsAsTol ]
    destroy $top
  }

  proc computeGeometry { reqGeom {parent .} } {
      if {[regexp {^(\d+)x(\d+)} $reqGeom => w h]} {
          set wp [winfo width $parent]
          set hp [winfo height $parent]
          set x [winfo rootx $parent]
          set y [winfo rooty $parent]
          set xpos "+[ expr {$x+($wp-$w)/2}]"
          set ypos "+[ expr {$y+($hp-$h)/2}]"
          return "${w}x${h}$xpos$ypos"
      } else {
          return ""
      }
  }

  typemethod DlgShow { objref args } {
    set opts(-title) [ mc "Set Editor" ]
    set opts(-modal) 1
    array set opts $args
    if { $opts(-modal) } {
      set top .dlgEditSet
      destroy .dlgEditSet
      if {[info exists opts(-geometry)]} {
          set newGeometry [ContainerEditor::computeGeometry $opts(-geometry)]
          if {$newGeometry eq ""} {
              set optGeom [list]
          } else {
              set optGeom [list -geometry $newGeometry]
          }
      } else {
          set optGeom [list]
      }
      Dialog $top -title $opts(-title) -parent . {*}$optGeom
      set f [ $top getframe ]
    } else {
      if { [ llength [ info command ::project::CreateForm ] ] } {
        set mdi [ ::project::CreateForm \
                      -title $opts(-title) \
                      -type EditForm -iniconfig EditForm ]
        set top $mdi
        set f [ $mdi getframe ]
      } else {
        set id [ incr topCounter ]
        set top .t$id
        destroy $top
        toplevel $top
        set f $top
      }
    }
    set _args {}
    foreach {o v} $args {
      if { $o eq "-title" || $o eq "-modal" || $o eq "-geometry"} continue
      if { $o eq "-address" } {
        set objref [ list "ADDRESS" $v ]
        continue
      }
      lappend _args $o $v
    }
    set edit [ eval ContainerEditor $f.edit -objref [ list $objref ] $_args ]
    grid $edit -row 0 -column 0 -sticky "snew"
    grid rowconfigure $f 0 -weight 1
    grid columnconfigure $f 0 -weight 1
    bind $edit <<OnAccept>> [ mytypemethod OnAccept $top $edit ]
    bind $edit <<OnCancel>> "destroy $top"
    if { $opts(-modal) } {
      $top draw
      #puts "modalResult = $modalResult"
      return $modalResult
    } else {
      return ""
    }
  }
}
