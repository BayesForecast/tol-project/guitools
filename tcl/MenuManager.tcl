package require Toltcl

namespace eval ::MenuManager {
  variable observers
  variable typesSelected
  variable submenuInfo
  variable mapdot [ list . -  :: -- ]
}

proc ::MenuManager::SelectionToSet { selection } {
  set items {}
  foreach a $selection {
    lappend items \"${a}\"
  }
  return "\[\[[ join $items , ]\]\]"
}

proc ::MenuManager::addObserver { event when script } {
  variable observers

  if { $when ne "Before" && $when ne "After" } {
    error "::MenuManager::addObserver -- valor invalido '$when' para el argumento when, debe ser Before o After"
  }
  lappend observers($event,$when) $script
}

proc ::MenuManager::invokeObservers { event when objSelection } {
  variable observers

  if { [ info exists observers($event,$when) ] } { 
    foreach script $observers($event,$when) {
      set result [ uplevel \#0 $script $when [ list $objSelection ] ]
      if { !$result } {
        return 0
      }
    }
  }
  return 1
}

proc ::MenuManager::checkEntryState { name objSelection args } {
  array set _args {
    -delegateOn ""
  }
  array set _args $args
  set SOA [ SelectionToSet $objSelection ]
  set tolexpr [ string map [ list %N $name %D $_args(-delegateOn) %S $SOA ] {
    Real {
      Set objects = EvalSet( %S, Anything( Text addr ) { 
        GetObjectFromAddress( addr )
      } );
      Real GuiTools::MenuManager::checkStateEntry( "%N", "%D", objects )
    }
  } ]
  return [ toltcl::eval $tolexpr ]
}

proc ::MenuManager::invokeCommand { name objSelection args } {
  if { ![ invokeObservers $name "Before" $objSelection ] } {
    return 0
  }
  array set _args {
    -delegateOn ""
  }
  array set _args $args
  set SOA [ SelectionToSet $objSelection ]
  set tolexpr [ string map [ list %N $name %D $_args(-delegateOn) %S $SOA ] {
    Real {
      Set objects = EvalSet( %S, Anything( Text addr ) { 
        GetObjectFromAddress( addr )
      } );
      Real GuiTools::MenuManager::invokeEntry( "%N", "%D", objects )
    }
  } ]
  set result [ toltcl::eval $tolexpr ]
  invokeObservers $name "After" $objSelection
  return $result  
}

proc ::MenuManager::getTypeOfObject { addr } {
  set tolexpr [ string map [ list %A $addr ] {
    Set GuiTools::MenuManager::getTypeOfObject( GetObjectFromAddress( "%A" ) )
  } ]
  return [ lindex [ toltcl::eval $tolexpr ] 0 ]
}

# retorna las opciones de menu definidas para el tipo dado
proc ::MenuManager::getMenuOptionsForType { type } {
  set tolexpr [ string map [ list %T $type ] {
    Set GuiTools::MenuManager::getTypeOptionsInfo( "%T" );
  } ]
  return [ lindex [ toltcl::eval $tolexpr -named 1 ] 1 ]
}

# retorna las opciones de menu definidas para el tipo dado
proc ::MenuManager::getMenuOptionsForObject { addr } {
  set tolexpr [ string map [ list %A $addr ] {
    Set GuiTools::MenuManager::getObjectOptionsInfo( GetObjectFromAddress( "%A" ) );
  } ]
  return [ lindex [ toltcl::eval $tolexpr -named 1 ] 1 ]
}

proc ::MenuManager::getDelegatedOptions { entryName addr } {
  set tolexpr [ string map [ list %E $entryName %A $addr ] {
    Set GuiTools::MenuManager::getDelegatedOptions( "%E", GetObjectFromAddress( "%A" ) );
  } ]
  return [ lindex [ toltcl::eval $tolexpr -named 1 ] 1 ]
}

proc ::MenuManager::getEntryInfo { id } {
  set tolexpr [ string map [ list %I $id ] {
    Set {
      Set aux = GuiTools::MenuManager::getEntryInfo( "%I" );
      Set If( Card( aux ),
              aux, GuiTools::MenuManager::getDefaultEntryInfo( "%I" ) )
    }
  } ]
  return [ lindex [ toltcl::eval $tolexpr -named 1 ] 1 ]
}

proc ::MenuManager::initTypesInfoFromSelection { selection } {
  variable typesSelected
  variable submenuInfo

  array unset typesSelected
  array set typesSelected {
    objects {}
    types {}
  }
  array unset submenuInfo
  foreach addr $selection {
    lappend typesSelected(objects) $addr
    set _type [ getTypeOfObject $addr ]
    # proceso tipo subtipo
    foreach type $_type {
      if { $type eq "" } {
        # es un NameBlock sin clase o Set sin estructura definida.
        continue
      }
      # pido las opciones de menu definidas para el tipo del objeto
      set typeOptionInfo [ getMenuOptionsForType $type ]
      # registro el tipo solo si tiene informacion de menu asociada
      if { [ llength $typeOptionInfo ] } {
        if { ![ info exists typesSelected(objects,$type) ] } {
          # registro la informacion de type, solo se hace para el primer
          # objeto visitado de ese tipo en la seleccion
          lappend typesSelected(types) $type
          # guardo la informacion de etiqueta para el tipo
          set typesSelected(labelInfo,$type) [ getEntryInfo $type ]
          # guardo las opciones de menu para este tipo
          set typesSelected(entriesInfo,$type) {}
          foreach {option optionInfo} $typeOptionInfo {
            lappend typesSelected(entriesInfo,$type) $optionInfo
            set typesSelected(optionInfo,$option) $optionInfo
            set entryPath [ split $option "/" ]
            if { [ llength $entryPath ] > 1 } {
              # esta entry va dentro de un submenu, le pido a
              # MenuManager la informacion de las entries contenidas
              # en el path de la opcion, la etiqueta de la opcion ya
              # esta definida en la opcion
              for {set im 0} {$im < [expr {[llength $entryPath]-1}]} {incr im} {
                set submenuID [join [lrange $entryPath 0 $im] "/"]
                if { ![ info exist typesSelected(labelInfo,$submenuID) ] } {
                  # evito multiples llamadas a TOL
                  set typesSelected(labelInfo,$submenuID) [ getEntryInfo $submenuID ]
                }
              }
            }
            # referencia cruzada, los tipos de la seleccion que
            # comparten una opcion
            if { [ catch { lsearch $typesSelected(types,$option) $type } idx ] } {
              # se inicializa la lista de tipos asociadas a la opcion
              set typesSelected(types,$option) [ list $type ]
            } elseif { $idx == -1 } {
              # type no esta en la lista de tipos asocidas a la opcion
              lappend typesSelected(types,$option) $type
            }
          }
        }
        lappend typesSelected(objects,$type) $addr
      }
    }
  }
  # busco las opciones que se aplican a toda la seleccion
  set numberOfTypes [ llength $typesSelected(types) ]
  set typesSelected(globalOptions) [ list ]
  foreach optionIndex [ array names typesSelected types,* ] {
    set optionName [ string range $optionIndex 6 end ]
    if { [ llength $typesSelected(types,$optionName) ] == $numberOfTypes } {
      lappend typesSelected(globalOptions) $typesSelected(optionInfo,$optionName)
    }
  }
  return $typesSelected(types)
}

proc ::MenuManager::compareMenuEntries { entryInfo1 entryInfo2 } {
  variable typesSelected

  array set entry1 $entryInfo1
  array set entry2 $entryInfo2

  set name1 $entry1(_.name)
  set name2 $entry2(_.name)
  set list1 [ split $name1 "/" ]
  set list2 [ split $name2 "/" ]
  set l1 [llength $list1]
  set l2 [llength $list2]
  if { $l1 < $l2 } {
    set l [incr l1 -1]
  } else {
    set l [incr l2 -1]
  }
  set key1 [join [lrange $list1 0 $l] "/"]
  set key2 [join [lrange $list2 0 $l] "/"]
  if {$key1 ne $name1} {
    array set entry1 $typesSelected(labelInfo,$key1)
  }
  if {$key2 ne $name2} {
    array set entry2 $typesSelected(labelInfo,$key2)
  }
  return [ expr { $entry1(_.rank) - $entry2(_.rank) } ]
}

proc ::MenuManager::insertEntries { menuWidget objSelection entriesInfo args } {
  variable typesSelected
  variable submenuData
  variable mapdot

  set multiple [ expr { [ llength $objSelection ] > 1 } ]
  # inserto las entries segun el orden inducido por el campo rank
  set sortedEntriesInfo [ lsort \
                              -command ::MenuManager::compareMenuEntries \
                              $entriesInfo ]
  array set _args {
    -level 0
    -delegateOn ""
  }
  array set _args $args
  foreach entryInfo $sortedEntriesInfo {
    array set entry {
      _.rank 0
      _.delegateOn {}
    }
    array set entry $entryInfo
    set name $entry(_.name)
    if { ( $entry(_.flagGroup) ^ $multiple ) &&
         ( $entry(_.delegateOn) eq "" ) } {
      continue
    }
    set entryPath [ split $name "/" ]
    if { [ llength $entryPath ] > [ expr { $_args(-level) + 1 } ] } {
      set submenuID [join [lrange $entryPath 0 $_args(-level)] "/"]
      set submenuW $menuWidget.subm[ string map $mapdot $submenuID ]
      if { ![ info exists submenuData($submenuW,entries) ] } {
        destroy $submenuW
        menu $submenuW -tearoff 0 \
            -postcommand [ list ::MenuManager::postSubmenu $submenuW -type explicit ]
        array set entrySub $typesSelected(labelInfo,$submenuID)
        set label $entrySub(_.label)
        set imageId $entrySub(_.image)
        set image0 [ ::ImageManager::getImageResourceId $imageId ]
        if { $image0 ne "" } {
          set image $image0
        } else {
          set image ""
        }
        $menuWidget add cascade -menu $submenuW \
            -label [ expr { $entrySub(_.flagTranslate) ? [ msgcat::mc $label ] : $label } ]\
            -image $image -compound [ expr { $image eq "" ? "none" : "left" } ]
        set submenuData($submenuW,entries) {}
        set submenuData($submenuW,posted) 0
        set submenuData($submenuW,selection) $objSelection
        set submenuData($submenuW,level) $_args(-level)
        set submenuData($submenuW,delegateOn) $_args(-delegateOn)
      }
      # guardo la entry del submenu para luego poder "postearla"
      #set entry(_.name) [ join [ lrange $entryPath 1 end ] "/" ]
      lappend submenuData($submenuW,entries) [ array get entry ]
    } elseif { $entry(_.delegateOn) ne "" } {
      # opcion de menu delegada
      set submenuW $menuWidget.subm[ string map $mapdot $entry(_.name) ]
      destroy $submenuW
      menu $submenuW -tearoff 0 \
          -postcommand [ list ::MenuManager::postSubmenu $submenuW -type delegated ]
      set submenuData($submenuW,posted) 0
      set submenuData($submenuW,delegated) $entryInfo
      set submenuData($submenuW,selection) $objSelection
      set label $entry(_.label)
      set imageId $entry(_.image)
      set image0 [ ::ImageManager::getImageResourceId $imageId ]
      if { $image0 ne "" } {
        set image $image0
      } else {
        set image ""
      }
      $menuWidget add cascade -menu $submenuW \
          -label [ expr { $entry(_.flagTranslate) ? [ msgcat::mc $label ] : $label } ]\
          -image $image -compound [ expr { $image eq "" ? "none" : "left" } ]
    } else {
      # es una opcion de menu normal
      set imageId $entry(_.image)
      set image0 [ ::ImageManager::getImageResourceId $imageId ]
      if { $image0 ne "" } {
        set image $image0
      } else {
        set image ""
      }
      set state [ checkEntryState $name $objSelection \
                      -delegateOn $_args(-delegateOn) ]
      set label $entry(_.label)
      $menuWidget add command \
          -label [ expr { $entry(_.flagTranslate) ? [ msgcat::mc $label ] : $label } ] \
          -command [ list ::MenuManager::invokeCommand $name $objSelection \
                         -delegateOn $_args(-delegateOn) ] \
          -state [ expr { $state ? "normal" : "disabled" } ] \
          -image $image -compound [ expr { $image eq "" ? "none" : "left" } ]
    }
  }
}

proc ::MenuManager::postSubmenu { menuW args } {
  variable submenuData

  array set _args $args

  if { $_args(-type) eq "explicit" } {
    if { !$submenuData($menuW,posted) } {
      insertEntries $menuW $submenuData($menuW,selection) \
          $submenuData($menuW,entries) \
          -level [ expr $submenuData($menuW,level) + 1 ] \
          -delegateOn $submenuData($menuW,delegateOn)
      
      set submenuData($menuW,posted) 1
    }
  } elseif { $_args(-type) eq "delegated" } {
    if { !$submenuData($menuW,posted) } {
      # solo hay una entry
      array set entry $submenuData($menuW,delegated)
      set entriesDelegated {}
      variable typesSelected
      foreach {n i} [ getDelegatedOptions $entry(_.name) [ lindex $submenuData($menuW,selection) 0 ] ] {
        # se revisan y construyen posibles etiquetas de submenues no
        # visitadas, esta situacion se puede dar solo en las opciones
        # delegadas.
        set entryPath [ split $n "/" ]
        if { [ llength $entryPath ] > 1 } {
          for {set im 0} {$im < [expr {[llength $entryPath]-1}]} {incr im} {
            set submenuID [join [lrange $entryPath 0 $im] "/"]
            if { ![ info exist typesSelected(labelInfo,$submenuID) ] } {
              # evito multiples llamadas a TOL
              set typesSelected(labelInfo,$submenuID) [ getEntryInfo $submenuID ]
            }
          }
        }
        lappend entriesDelegated $i
      }
      insertEntries $menuW $submenuData($menuW,selection) $entriesDelegated \
          -delegateOn $entry(_.delegateOn)
      set submenuData($menuW,posted) 1
    }
  } else {
    error "unknown submenu type '$_args(-type)'"
  }
}

proc ::MenuManager::insertEntriesForSelection { menuWidget selection } {
  variable typesSelected
  variable submenuData
  variable mapdot

  array unset submenuData

  # recojo informacion de menu contextual asociada a la seleccion
  if { ![ llength [ initTypesInfoFromSelection $selection ] ] } return
  # lista de objetos sobre los cuales hay opciones de menu definida
  set lenghtValidSelection [ llength $typesSelected(objects) ]
  set lengthSelection [ llength $selection ]
  if { $lengthSelection == $lenghtValidSelection } {
    # inserto opciones que se aplican a toda la seleccion
    # independiente del tipo
    insertEntries $menuWidget \
        $typesSelected(objects) $typesSelected(globalOptions)
  }
  # inserto las opciones especificas de cada tipo si es que hay mas de
  # un tipo
  if { ( $lengthSelection != $lenghtValidSelection ) ||
       [ llength $typesSelected(types) ] > 1 } {
    # reemplazo caracteres invalidos en el nombre de las entries
    foreach type $typesSelected(types) {
      # type puede contener {. ::} por eso hay que reemplazarlo
      set type0 [ string map $mapdot $type ]
      set menuType $menuWidget.mtype$type0
      if { ![ winfo exists $menuType ] } {
        menu $menuType -tearoff 0
      } else {
        $menuType delete 0 end
      }
      array set labelInfo $typesSelected(labelInfo,$type);
      set label $labelInfo(_.label)
      set labelCascade [ expr { $labelInfo(_.flagTranslate) ? 
                                [ msgcat::mc $label ] : $label } ]
      set imageId $labelInfo(_.image)
      if { $imageId ne "" } {
        set image [ ::ImageManager::getImageResourceId $imageId ]
      } else {
        set image ""
      }
      $menuWidget add cascade -label $labelCascade -menu $menuType \
          -image $image -compound [ expr { $image eq "" ? "none" : "left" } ]
      insertEntries $menuType \
          $typesSelected(objects,$type) $typesSelected(entriesInfo,$type)
    }
  }
}
