namespace eval GuiTools {
  variable dir [ file normalize [ file dirname [ info script ] ] ]
}

source [ file join $GuiTools::dir "MultiDimSelect.tcl" ]
source [ file join $GuiTools::dir "MenuManager.tcl" ]
source [ file join $GuiTools::dir "ImageManager.tcl" ]
source [ file join $GuiTools::dir "WaterfallChart.tcl" ]
source [ file join $GuiTools::dir "ContainerEditor.tcl" ]
