package require BWidget

namespace eval ::ImageManager {
}

proc ::ImageManager::createImageFromData { imageName imageData } {
  image create photo -data $imageData
}

proc ::ImageManager::createImageFromFile { imageName imagePath } {
  #puts "::ImageManager::createImageFromFile $imageName $imagePath"
  set img [ image create photo ]
  set items [ file split $imagePath ]
  if { [ llength $items ] == 1 } {
    set img [ ::Bitmap::get [ file rootname $imagePath ] ]
  } else {
    $img read $imagePath
  }
  return $img
}

proc ::ImageManager::getImageResourceId { imageName } {
  set rvar "__getImageResourceId__"
  set tolExpr [ string map [ list %N $imageName %RV $rvar ] {
    Text %RV = {
      Real test = ObjectExist( "Code", 
                               "GuiTools::ImageManager::getImageResourceId" );
      Text If( test, GuiTools::ImageManager::getImageResourceId( "%N" ), "" )
    }
  } ]
  tol::console eval $tolExpr
  set info [ tol::info variable [ list Text $rvar ] ]
  set value [ string trim [ lindex $info 2 ] \" ]
  tol::console stack release $rvar
  return $value
}

proc ::ImageManager::getIconForInstance { objAddress } {
  set rvar "__getIconForInstance__"
  set tolExpr [ string map [ list %A $objAddress %RV $rvar ] {
    Text %RV = {
      Real test = ObjectExist( "Code", 
                               "GuiTools::ImageManager::getIconForInstance" );
      Text If( test, {
        NameBlock obj = GetObjectFromAddress( "%A" );
        // obtengo el nombre del imagen
        Text imageName = GuiTools::ImageManager::getIconForInstance( obj );
        // obtengo el id del recurso que es el id de la imagen Tk
        Text GuiTools::ImageManager::getImageResourceId( imageName )
      }, "" )
    }
  } ]
  tol::console eval $tolExpr
  set info [ tol::info variable [ list Text $rvar ] ]
  set value [ string trim [ lindex $info 2 ] \" ]
  tol::console stack release $rvar
  return [ expr {$value eq "" ? [::Bitmap::get "NameBlock"] : $value} ]
}

proc ::ImageManager::getIconForClass { className } {
  set rvar "__getIconForClass__"
  set tolExpr [ string map [ list %C $className %RV $rvar ] {
    Text %RV = {
      Real test = ObjectExist( "Code", 
                               "GuiTools::ImageManager::getIconValueForClass" );
      Text If( test, {
        // obtengo el nombre del imagen
        Text imageName = GuiTools::ImageManager::getIconValueForClass( "%C" );
        // obtengo el id del recurso que es el id de la imagen Tk
        Text GuiTools::ImageManager::getImageResourceId( imageName )
      }, "" )
    }
  } ]
  tol::console eval $tolExpr
  set info [ tol::info variable [ list Text $rvar ] ]
  set value [ string trim [ lindex $info 2 ] \" ]
  tol::console stack release $rvar
  return [ expr {$value eq "" ? [::Bitmap::get "NameBlock"] : $value} ]
}
