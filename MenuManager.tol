NameBlock MenuManager =
[[
  // contenedor de opciones, son instancias de @MenuOption
  Set _.options = Copy( Empty );
  // asociacion explicitas entre clases y opciones de menu
  Set _.explicit_assoc = Copy( Empty );

  Real permisiveDefine = 1;

#Embed "MenuOption.tol";

  Real removeModule( Text module )
  {
    Set _.options := Select(_.options, 
        Real (NameBlock opt) { Not(TextMatch(Name(opt), module+".*")) });
    Set _.explicit_assoc := Select(_.explicit_assoc, 
        Real (Set assoc) { Not(TextMatch(Name(assoc), module+"::*")) });
  1};
  
  Real defineTypeLabel( Text typeName, NameBlock args )
  {
    Warning( "DEPRECATED (defineTypeLabel): use defineOptionLabel or replaceOptionLabel instead of defineTypeLabel" );
    Set _args = NameBlockToSet( args );
    Set Append( _args, [[ Text name = typeName ]], 1 );
    Real defineOptionLabel( SetToNameBlock( _args ) )
  };

  Real defineOptionLabel( NameBlock args )
  {
    Real idx = findOption( args::name );
    If( idx, {
        Warning( "La opcion " + args::name + " ya esta definida" );
        idx
       }, {
        @MenuOption option = @MenuOption::New( args );
        Real _addOption( option )
      } )
  };

  Real replaceOptionLabel( NameBlock args )
  {
    Real idx = findOption( args::name );

    Real If( idx, {
        // si la opcion existe se reemplaza su definicion
        @MenuOption opt = getOption( idx );
        opt::replace( args );
        idx
      }, {
        // si la opcion no existe se crea una nueva
        @MenuOption opt = @MenuOption::New( args );
        Real _addOption( opt )
      } )
  };

  Real defineMenuCommand( Text typeName, NameBlock args )
  {
  //WriteLn("TRACE [GuiTools::MenuManager::defineMenuCommand] 1 args::name='"<<args::name+"'");
    Real _idx = findOption( args::name );
  //WriteLn("TRACE [GuiTools::MenuManager::defineMenuCommand] 2 args::name='"<<args::name+"'");
    Real idx = If( _idx,  {
  //WriteLn("TRACE [GuiTools::MenuManager::defineMenuCommand] 3 args::name='"<<args::name+"'");
        // verifico que no sea una reutilizacion de una opcion
        // definida en otro type
        If( Not(permisiveDefine) & ( Card( NameBlockToSet( args ) ) > 1 ), {
            Warning( "El comando de menu '" + args::name +
                     "' ya está definido. Utilice replaceMenuCommand si quiere redefinirla." )
          } );
        _idx
      }, {
        // si la opcion no existe se crea una nueva y se inserta en el
        // contenedor global de opciones.
        @MenuCommand opt = @MenuCommand::New( args );
        Real _addOption( opt )
      } );
    // asociamos la opcion a la clase, esto es para acceder en tiempo
    // constante a todas las opciones definidas para un tipo
    _assocOptionToType( typeName, idx );
    Real idx
  };

  Real replaceMenuCommand( Text typeName, NameBlock args )
  {
    Real _idx = findOption( args::name );

    Real idx = If( _idx, {
        // verifico que no sea una reutilizacion de una opcion
        // definida en otro type
        If( Card( NameBlockToSet( args ) ) > 1, {
            // si la opcion existe se reemplaza su definicion
            @MenuCommand opt = getOption( _idx );
            opt::replace( args )
          } );
        _idx
      }, {
        // si la opcion no existe se crea una nueva
        @MenuCommand opt = @MenuCommand::New( args );
        Real _addOption( opt )
      } );
    // asociamos la opcion a la clase, esto es para acceder en tiempo
    // constante a todas las opciones definidas para un tipo
    _assocOptionToType( typeName, idx );
    Real idx
  };

  Real findOption( Text optionName )
  {
    Real FindIndexByName( _.options, optionName )
  };

  Set getEntryInfo( Text optionName )
  {
    Real idx = findOption( optionName );
    Set If( idx, {
        NameBlock option = getOption( idx );
        Set option::getEntryInfo(?)
      }, Copy( Empty ) )
  };

  Set getDefaultEntryInfo( Text optionName )
  {
    Set @MenuOption::getDefaultEntryInfo( optionName )
  };
  
  Real invokeEntry( Text optionName, Text delegateOn,
                    Set objOrSelection )
  {
    Real idx = findOption( optionName );
    If( idx, {
        @MenuCommand option = getOption( idx );
        // Set View( [[ option ]], "Std" );
        Real option::invoke( delegateOn,
                             If( option::isMultiple(?),
                                 objOrSelection, objOrSelection[ 1 ] ) )
      }, 0 )
  };

  Real checkStateEntry( Text optionName, Text delegateOn,
                        Set objOrSelection )
  {
    Real idx = findOption( optionName );
    If( idx, {
        @MenuCommand option = getOption( idx );
        Real option::checkState( delegateOn,
                                 If( option::isMultiple(?),
                                 objOrSelection, objOrSelection[ 1 ] ) )
      }, 0 )
  };

  Set getTypeOptionsInfo( Text typeName )
  {
    // _getTypeAssociation solo retorna las opciones explicitas
    Set result =
      EvalSet( _getTypeAssociation( typeName ), Set( @MenuCommand entry ) {
          Set entry = getEntryInfo( entry::_.name );
          { PutName(entry::_.name,entry) }
        } )
  };
  
  Set getObjectOptionsInfo( Anything obj )
  {
    // _getTypeAssociation solo retorna las opciones explicitas
    Set typeInfo = getTypeOfObject( obj );
    Text typeName = typeInfo[ Card( typeInfo ) ];
    Set getTypeOptionsInfo( typeName )
  };

  Set getDelegatedOptions( Text optionName, NameBlock object )
  {
    Real idx = findOption( optionName );
    Set If( idx, {
        @MenuCommand option = getOption( idx );
        Set getObjectOptionsInfo( option::getDelegateOnTarget( object ) )
      }, Empty )
  };

  Set getTypeOfObject( Anything obj )
  {
    Text theGrammar = Grammar( obj );
    Set Case( theGrammar == "NameBlock", {
        [[ "NameBlock", ClassOf( obj ) ]]
      }, theGrammar == "Set", {
        [[ "Set ", StructName( obj ) ]]
      }, 1, [[ theGrammar ]] )
  };
  
  @MenuOption getOption( Real idx )
  {
    _.options[ idx ]
  };

  Real defineDelegatedSubmenus( Real void )
  {
    Warning( "DEPRECATED (defineDelegatedSubmenus): in order to delegate menu options you should use the field 'delegateOn' when invoking defineMenuCommand or replaceMenuCommand. You should update your use of GuiTools to hold at least 3.5 requirements." );
    Real 1
  };
   
  Real delegateSubmenu( Text typeName, Text memberClass,
                        Text memberAccess, NameBlock args )
  {
    Warning( "DEPRECATED (delegateSubmenu): in order to delegate menu options you should use the field 'delegateOn' when invoking defineMenuCommand or replaceMenuCommand. You should update your use of GuiTools to at hold least 3.5 requirements." );
    Real 1
  };

  // private methods

  Real _addOption( @MenuOption option )
  {
    NameBlock aux = option;
    Text name = option::_.name;
    Set Append( _.options, { [[ PutName(name,aux ) ]] }, True );
    Real Card( _.options )
  };

  Real _assocOptionToType( Text typeName, Real idx )
  {
    NameBlock opt = _.options[ idx ];
    Set typeOptions = _getTypeAssociation( typeName );
    Real idx0 = FindIndexByName( typeOptions, opt::_.name );
    If( idx0, idx0, {
        // si la opcion no esta asociada al type se inserta con el
        // nombre de la opcion
        Set Append( typeOptions, [[ _.options[ idx ] ]], True );
        Real Card( typeOptions )
      } )
  };

  Set _getTypeAssociation( Text typeName )
  {
    Real idxType = FindIndexByName( _.explicit_assoc, typeName );
    Set If( idxType, _.explicit_assoc[ idxType ],
            _createTypeAssociation( typeName ) )
  };

  Set _createTypeAssociation( Text typeName )
  {
    Set aux = Copy( Empty );
    Set Append( _.explicit_assoc, { [[ PutName(typeName,aux) ]] }, True );
    Set _.explicit_assoc[ Card( _.explicit_assoc ) ]
  }

]];

