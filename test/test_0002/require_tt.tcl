package require Toltcl

if { [ info command tol::initlibrary ] eq "" } {
  proc Tol_HciWriter { msg } {
    puts [ string trim $msg ]
  }

  tol::initkernel 
  tol::initlibrary 0
}
