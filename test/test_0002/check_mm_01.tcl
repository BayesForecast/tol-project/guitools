source check_mm_00.tcl

foreach a $selection {
  puts [ ::MenuManager::getTypeOfObject $a ]
}

puts "==== ::MenuManager::getLabelInfoForType Test02::@Ejemplos ===="
puts [ ::MenuManager::getMenuOptionsForType "Test02::@Ejemplos" ]
puts ""

set a [ lindex $selection 0 ]
set type1 [ lindex [ ::MenuManager::getTypeOfObject $a ] end ]

puts "==== ::MenuManager::getMenuOptionsForType $type1 ===="
puts [ ::MenuManager::getMenuOptionsForType $type1 ]
puts ""

puts "==== ::MenuManager::getMenuOptionsForObject $a ===="
puts [ ::MenuManager::getMenuOptionsForObject $a ]
puts ""

puts "==== ::MenuManager::getEntryInfo $type1 ===="
puts [ ::MenuManager::getEntryInfo $type1 ]
puts ""

puts "==== ::MenuManager::getDelegatedOptions ===="
puts [ ::MenuManager::getDelegatedOptions "Ejemplos.Ejemplo1" \
           [ tol::info address [ list NameBlock ejemplos1 ] ]]
puts "===="

puts [ ::MenuManager::initTypesInfoFromSelection $selection ]

#parray ::MenuManager::typesSelected